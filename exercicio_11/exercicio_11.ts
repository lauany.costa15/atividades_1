//Comentar
/*
Faça um programa que receba um número positivo e
maior que zero, calcule e mostre:
a) O número digitado ao quadrado;
b) O número digitado ao cubo;
c) A raiz quadrada do número digitado;
d) A raiz cúbica de número digitado.
*/

//Inicio
namespace exercicio_11 {
  //Entrada de dados
  const num = 7;
  
  let numQ: number;

  numQ = num * num;
  numQ = Math.pow(num, 2);
  numQ = num ** 2;

  let numC: number;

  numC = num * num * num;
  numC = Math.pow(num, 3);
  numC = num ** 3;

  let raizQ: number;

  raizQ = Math.sqrt(num);

  let raizC: number;

  raizC = Math.cbrt(num);

  console.log(
    `O número elevado ao quadrado: ${numQ} \n O número elevado ao cubo: ${numC} \n A raiz quadrada do número ${raizQ} \n A raiz cubica do numero ${raizC}`
  );
}
